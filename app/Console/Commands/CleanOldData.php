<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Repositories\GameSessionRepository;
use Carbon\Carbon;

class CleanOldData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'olddata:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove records older than 1 weeks';

    protected $connection;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->connection = DB::connection();
        $this->gameSessionRepo = new GameSessionRepository();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$this->gameSessionRepo->deleteExpired();
        DB::table('players')->where('created_at', '<', Carbon::now()->subWeek())->delete();
    }
}
