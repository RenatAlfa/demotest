<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$app->withFacades();

$app->post('/callback/session','CallbackController@actionGetSession');
$app->post('/callback/session/create', 'CallbackController@actionCreateSession');
$app->post('/callback/bet', 'CallbackController@actionBet');
$app->post('/callback/win', 'CallbackController@actionWin');
$app->post('/callback/refund', 'CallbackController@actionRefund');
$app->post('/callback/balance', 'CallbackController@actionBalance');
$app->post('/callback/close', 'CallbackController@actionCloseSession');
$app->post('/demogame', 'CallbackController@actionDemoGame');