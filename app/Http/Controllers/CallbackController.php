<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Support\Facades\Response;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Exception\HttpResponseException;

use Log;
use DB;
use Carbon\Carbon;
use App\Repositories\GameSessionRepository;
use Validator;

class CallbackController extends BaseController
{
    protected $gameSessionRepo;

    function __construct()
    {
        $this->gameSessionRepo = new GameSessionRepository;
    }

    public function actionDemoGame(Request $request)
    {
        $gameSessionRepo = $this->gameSessionRepo;

        $this->validate($request, [
            'balance'  => 'required|integer',
            'game_id'  => 'required',
            'currency' => 'required|in:RUB',
            'sign'     => 'required',
        ]);
        $this->validateSign($request->all());
        $balance = $request->input('balance');
        $playerUUID = Uuid::uuid4()->toString();
        $gameUUID = $request->input('game_id');
        $currency = $request->input('currency');

        DB::insert('insert into `players`(`uuid`,`balance`,`created_at`) values (:uuid,:balance,:created_at)', [
            'uuid'       => $playerUUID,
            'balance'    => $balance,
            'created_at' => new Carbon(),

        ]);

        $player = DB::select('select * from `players` where `uuid`=:uuid', [
            'uuid' => $playerUUID,
        ]);
        $game = DB::select('select * from `games` where `uuid`=:uuid', [
            'uuid' => $gameUUID,
        ]);

        if (null == $player || null == $game)
        {
            return response()->json(['msg' => 'something went wrong'], 500);
        }

        $player = $player[0];
        $game = $game[0];

        $sessionUUID = $gameSessionRepo->create($player->id, $game->id, $currency);
        if ($sessionUUID === false)
        {
            return response()->json(['msg' => 'something went wrong'], 500);
        }

        $merchantID = env('BACKOFFICE_MERCHANT_ID');
        $exitURI = 'http://google.com/';

        $params = [
            'token'      => $sessionUUID,
            'merchantID' => $merchantID,
            'exit'       => $exitURI,
            'demo'       => 1,
        ];

        $this->createSign($params);

        return response()->json([
            'url'   => env('BACKOFFICE_HOST') . '/game' . '?' . http_build_query($params),
            'token' => $sessionUUID,
        ], 200);
    }

    public function actionGetSession(Request $request)
    {

        $gameSessionRepo = $this->gameSessionRepo;

        $this->validate($request, [
            'token' => 'required',
            'sign'  => 'required',
        ]);

        $this->validateSign($request->all());
        $sessionUUID = $request->input('token');
        $session = $gameSessionRepo->getSession($sessionUUID);

        if ($session === false)
        {
            return response()->json(
                [
                    'msg' => 'session not found',
                ], 404);
        }

        $balance = $this->getBalance($session['player_id']);

        $result = [
            'playerId' => $session['player_uuid'],
            'gameId'   => $session['game_uuid'],
            'currency' => $session['currency'],
            'balance'  => $balance / 100,
        ];

        return response()->json($result, 200);
    }

    public function actionCreateSession(Request $request)
    {
        $gameSessionRepo = $this->gameSessionRepo;

        $this->validate($request, [
            'player_id' => 'required|integer',
            'game_id'   => 'required|integer',
            'currency'  => 'required|in:RUB',
        ]);

        $currency = $request->input('currency');

        $player = DB::select('select * from `players` where `id`=:id', [
            'id' => $request->input('player_id'),
        ]);

        $game = DB::select('select * from `games` where `id`=:id', [
            'id' => $request->input('game_id'),
        ]);

        if (null == $player || null == $game)
        {
            return response()->json([
                'msg' => 'user or game not found',
            ], 404);
        }

        $player = $player[0];
        $game = $game[0];

        $sessionUUID = $gameSessionRepo->create($player->id, $game->id, $currency);

        if ($sessionUUID === false)
        {
            return response()->json([
                'msg' => 'something went wrong',
            ], 500);
        }

        $merchantID = env('BACKOFFICE_MERCHANT_ID');
        $exitURI = 'http://google.com/';

        $params = [
            'token'      => $sessionUUID,
            'merchantID' => $merchantID,
            'exit'       => $exitURI,
        ];

        $this->createSign($params);


        return redirect()->to(env('BACKOFFICE_HOST') . '/game' . '?' . http_build_query($params));
    }

    public function actionCloseSession(Request $request)
    {
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' REQUEST: ', $request->all());
        
        $this->validate($request, [
            'count'     => 'required',
            'timestamp' => 'required',
            'sign'      => 'required',
        ]);

        $this->validateSign($request->only(['count', 'timestamp', 'sign']));

        $transactions = $request->except(['count', 'timestamp', 'sign']);

        $response = [];

        foreach ($transactions as $item)
        {
            $response[] = [
                'transactionId'    => $item['transactionId'],
                'alreadyProcessed' => false,
                'balance'          => $item['balance'],
                'code'             => 1200,
            ];
        }

        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' RESPONSE:', $response);
        return response()->json($response, 200);
    }

    public function actionBet(Request $request)
    {
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' REQUEST: ', $request->all());

        $gameSessionRepo = $this->gameSessionRepo;
        $this->validate($request, [
            'count'     => 'required',
            'timestamp' => 'required',
            'sign'      => 'required',
        ]);
        $this->validateSign($request->only(['count', 'timestamp', 'sign']));

        $date = $request->input('timestamp');
        $transactionCount = $request->input('count');
        $transactions = $request->except(['count', 'timestamp', 'sign']);

        $response = [];
        for ($currentTransaction = 0; $currentTransaction < $transactionCount; $currentTransaction++)
        {
            $transaction = $transactions[$currentTransaction];
            $session = $gameSessionRepo->getSession($transaction['token']);
            $transactionID = $transaction['transactionId'];

            if ($session === false)
            {
                $response[] = [
                    'transactionId'    => $transactionID,
                    'balance'          => 0,
                    'alreadyProcessed' => 'false',
                    'code'             => 1404,
                ];
                continue;
            }

            $processedTransaction = $this->getTransaction($transactionID);

            if ($processedTransaction !== false)
            {
                $response[] = [
                    'transactionId'    => $transactionID,
                    'balance'          => $processedTransaction->balance / 100,
                    'alreadyProcessed' => 'true',
                    'code'             => 1200,
                ];
                continue;
            }

            $betAmount = $transaction['amount'] * 100;
            $currentBalance = $this->getBalance($session['player_id']);

            if ($currentBalance < $betAmount)
            {
                $response[] = [
                    'transactionId'    => $transactionID,
                    'balance'          => $currentBalance / 100,
                    'alreadyProcessed' => 'false',
                    'code'             => 1402,
                ];
                continue;
            }

            $newBalance = $currentBalance - $betAmount;

            DB::transaction(function () use ($transactionID, $session, $betAmount, $date, $newBalance)
            {
                $this->addTransaction([
                    'transaction_id' => $transactionID,
                    'session_id'     => $session['uuid'],
                    'player_id'      => $session['player_id'],
                    'amount'         => $betAmount,
                    'balance'        => $newBalance,
                    'type'           => 'BET',
                    'timestamp'      => date('Y-m-d H:i:s', $date),
                ]);
                $this->decreaseBalance($session['player_id'], $betAmount);
            });

            $response[] = [
                'transactionId'    => $transactionID,
                'balance'          => $newBalance / 100,
                'alreadyProcessed' => 'false',
                'code'             => 1200,
            ];
        }
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' RESPONSE:', $response);
        return response()->json($response, 200);
    }

    public function actionWin(Request $request)
    {
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' REQUEST: ', $request->all());
        $gameSessionRepo = $this->gameSessionRepo;


        $this->validate($request, [
            'count'     => 'required',
            'timestamp' => 'required',
            'sign'      => 'required',
        ]);
        $this->validateSign($request->only(['count', 'timestamp', 'sign']));

        $date = $request->input('timestamp');
        $transactionCount = $request->input('count');
        $transactions = $request->except(['count', 'timestamp', 'sign']);

        $response = [];
        for ($currentTransaction = 0; $currentTransaction < $transactionCount; $currentTransaction++)
        {
            $transaction = $transactions[$currentTransaction];
            $session = $gameSessionRepo->getSession($transaction['token']);
            $transactionID = $transaction['transactionId'];

            if ($session === false)
            {
                $response[] = [
                    'transactionId'    => $transactionID,
                    'balance'          => 0,
                    'alreadyProcessed' => 'false',
                    'code'             => 1404,
                ];
                continue;
            }
            $processedTransaction = $this->getTransaction($transactionID);

            if ($processedTransaction !== false)
            {
                $response[] = [
                    'transactionId'    => $transactionID,
                    'balance'          => $processedTransaction->balance / 100,
                    'alreadyProcessed' => 'true',
                    'code'             => 1200,
                ];
                continue;
            }

            $winAmount = $transaction['amount'] * 100;
            $currentBalance = $this->getBalance($session['player_id']);
            $newBalance = $currentBalance + $winAmount;

            DB::transaction(function () use ($transactionID, $session, $winAmount, $date, $newBalance)
            {
                $this->addTransaction([
                    'transaction_id' => $transactionID,
                    'session_id'     => $session['uuid'],
                    'player_id'      => $session['player_id'],
                    'amount'         => $winAmount,
                    'balance'        => $newBalance,
                    'type'           => 'WIN',
                    'timestamp'      => date('Y-m-d H:i:s', $date),
                ]);
                $this->increaseBalance($session['player_id'], $winAmount);
            });

            $response[] = [
                'transactionId'    => $transactionID,
                'balance'          => $newBalance / 100,
                'alreadyProcessed' => 'false',
                'code'             => 1200,
            ];
        }
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' RESPONSE:', $response);
        return response()->json($response, 200);
    }

    public function actionRefund(Request $request)
    {
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' REQUEST: ', $request->all());
        $gameSessionRepo = $this->gameSessionRepo;

        $this->validate($request, [
            'count'     => 'required',
            'timestamp' => 'required',
            'sign'      => 'required',
        ]);
        $this->validateSign($request->only(['count', 'timestamp', 'sign']));

        $date = $request->input('timestamp');
        $transactionCount = $request->input('count');
        $transactions = $request->except(['count', 'timestamp', 'sign']);

        $response = [];
        for ($currentTransaction = 0; $currentTransaction < $transactionCount; $currentTransaction++)
        {
            $transaction = $transactions[$currentTransaction];

            $session = $gameSessionRepo->getSession($transaction['token']);
            $transactionID = $transaction['transactionId'];

            if ($session === false)
            {
                $response[] = [
                    'transactionId'    => $transactionID,
                    'balance'          => 0,
                    'alreadyProcessed' => 'false',
                    'code'             => 1404,
                ];
                continue;
            }


            $processedTransaction = $this->getTransaction($transactionID);

            if ($processedTransaction !== false)
            {
                $response[] = [
                    'transactionId'    => $transactionID,
                    'balance'          => $processedTransaction->balance / 100,
                    'alreadyProcessed' => 'true',
                    'code'             => 1200,
                ];
                continue;
            }


            $betTransactionID = $transaction['betTransactionId'];
            $processedBetTransaction = $this->getTransaction($betTransactionID);
            $currentBalance = $this->getBalance($session['player_id']);
            $betAmount = $transaction['amount'] * 100;

            if ($processedBetTransaction === false)
            {
                $newBalance = $currentBalance;
                DB::transaction(function () use ($transactionID, $session, $betAmount, $date, $newBalance)
                {
                    $this->addTransaction([
                        'transaction_id' => $transactionID,
                        'session_id'     => $session['uuid'],
                        'player_id'      => $session['player_id'],
                        'amount'         => $betAmount,
                        'balance'        => $newBalance,
                        'type'           => 'BET',
                        'timestamp'      => date('Y-m-d H:i:s', $date),
                    ]);
                    $this->addTransaction([
                        'transaction_id' => $transactionID,
                        'session_id'     => $session['uuid'],
                        'player_id'      => $session['player_id'],
                        'amount'         => $betAmount,
                        'balance'        => $newBalance,
                        'type'           => 'REFUND',
                        'timestamp'      => date('Y-m-d H:i:s', $date),
                    ]);
                });
            } else
            {
                $newBalance = $currentBalance + $betAmount;
                DB::transaction(function () use ($transactionID, $session, $betAmount, $date, $newBalance)
                {
                    $this->addTransaction([
                        'transaction_id' => $transactionID,
                        'session_id'     => $session['uuid'],
                        'player_id'      => $session['player_id'],
                        'amount'         => $betAmount,
                        'balance'        => $newBalance,
                        'type'           => 'REFUND',
                        'timestamp'      => date('Y-m-d H:i:s', $date),
                    ]);
                    $this->increaseBalance($session['player_id'], $betAmount);
                });
            }

            $response[] = [
                'transactionId'    => $transactionID,
                'balance'          => $newBalance / 100,
                'alreadyProcessed' => 'false',
                'code'             => 1200,
            ];
        }
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' RESPONSE:', $response);
        return response()->json($response, 200);
    }

    public function actionBalance(Request $request)
    {
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' REQUEST: ', $request->all());
        $gameSessionRepo = $this->gameSessionRepo;

        $this->validate($request, [
            'count'     => 'required',
            'timestamp' => 'required',
            'sign'      => 'required',
        ]);
        $this->validateSign($request->only(['count', 'timestamp', 'sign']));
        $transactionCount = $request->input('count');
        $transactions = $request->except(['count', 'timestamp', 'sign']);
        $response = [];
        for ($currentTransaction = 0; $currentTransaction < $transactionCount; $currentTransaction++)
        {
            $transaction = $transactions[$currentTransaction];

            $session = $gameSessionRepo->getSession($transaction['token']);
            $transactionID = $transaction['transactionId'];

            if ($session === false)
            {
                $response[] = [
                    'transactionId' => $transactionID,
                    'balance'       => 0,
                    'code'          => 1404,
                ];
                continue;
            }

            $currentBalance = $this->getBalance($session['player_id']);
            $response[] = [
                'transactionId' => $transactionID,
                'balance'       => $currentBalance / 100,
                'code'          => 1200,
            ];
        }
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' RESPONSE:', $response);
        return response()->json($response, 200);
    }

    private function getTransaction($transaction_id)
    {
        $transaction = DB::select('select * from `transactions` where `transaction_id`=:transaction_id', [
            'transaction_id' => $transaction_id,
        ]);
        if (!empty($transaction))
        {
            return $transaction[0];
        }
        return false;
    }

    private function getBalance($player_id)
    {
        $query = DB::select('select `balance` from `players` where `id`=:player_id', [
            'player_id' => $player_id,
        ]);

        return $query[0]->balance;
    }

    private function validateSign(array $params)
    {
        $paramSign = $params['sign'];
        ksort($params);
        unset($params['sign']);
        $checkSign = sha1(implode('', $params) . env('API_SECRET_KEY'));

        if ($paramSign !== $checkSign)
        {
            throw new HttpResponseException(response()->json([
                'message' => 'The authentication credentials for the API are incorrect',
            ], 401));
        }
    }

    private function createSign(&$params)
    {
        ksort($params);
        $params['sign'] = sha1(implode('', $params) . env('API_SECRET_KEY'));
    }

    private function increaseBalance($player_id, $amount)
    {
        return DB::update('update `players` set `balance`=`balance`+:amount where `id`=:player_id', [
            'amount'    => $amount,
            'player_id' => $player_id,
        ]);
    }

    private function decreaseBalance($player_id, $amount)
    {
        return DB::update('update `players` set `balance`=`balance`-:amount where `id`=:player_id', [
            'amount'    => $amount,
            'player_id' => $player_id,
        ]);
    }

    private function addTransaction($params)
    {
        return DB::insert('insert into `transactions`(`transaction_id`,`session_id`,`player_id`,`amount`,`balance`,`type`,`timestamp`) values (:transaction_id,:session_id,:player_id,:amount,:balance,:type,:timestamp)', $params);
    }
}

