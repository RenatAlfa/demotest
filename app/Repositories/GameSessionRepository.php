<?php
namespace App\Repositories;

use DB;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

class GameSessionRepository
{

    protected $connection;

    protected $table;

    protected $expires;

    function __construct()
    {
        $this->table = 'game_sessions';
        $this->expires = 300;
        $this->connection = DB::connection();
    }

    public function create($playerID, $gameID, $currency)
    {
        $sessionUUID = Uuid::uuid4()->toString();

        $insertOK = $this->getTable()->insert($this->getPayload($sessionUUID, $playerID, $gameID, $currency));

        return $insertOK ? $sessionUUID : false;
    }

    public function getSession($sessionUUID)
    {
        $session = (array)$this->getTable()
            ->select(
                DB::raw('
            `game_sessions`.`uuid`,
            `game_sessions`.`currency`,
            `game_sessions`.`created_at`,
            `games`.`uuid` as `game_uuid`,
            `players`.`uuid` as `player_uuid`,
            `players`.`id` as `player_id`'))
            ->where('game_sessions.uuid', $sessionUUID)
            ->join('players', 'players.id', '=', 'game_sessions.player_id')
            ->join('games', 'games.id', '=', 'game_sessions.game_id')
            ->first();

        if ($session && !$this->sessionExpired($session))
        {
            $this->sessionUpdate($sessionUUID);
            return $session;
        }

        return false;
    }

    public function exists($sessionUUID)
    {
        $session = (array)$this->getTable()
            ->where('uuid', $sessionUUID)->first();

        return $session && !$this->sessionExpired($session);
    }

    public function sessionUpdate($sessionUUID)
    {
        $this->getTable()
            ->where('uuid', $sessionUUID)
            ->update(['created_at' => new Carbon]);
    }

    protected function sessionExpired($session)
    {
        $expirationTime = strtotime($session['created_at']) + $this->expires;

        return $expirationTime < $this->getCurrentTime();
    }

    protected function getPayload($UUID, $playerID, $gameID, $currency)
    {
        return ['uuid' => $UUID, 'player_id' => $playerID, 'game_id' => $gameID, 'currency' => $currency, 'created_at' => new Carbon];
    }

    protected function getTable()
    {
        return $this->connection->table($this->table);
    }

    public function getConnection()
    {
        return $this->connection;
    }

    protected function getCurrentTime()
    {
        return time();
    }

    public function deleteExpired()
    {
        $expiredAt = Carbon::now()->subSeconds($this->expires);

        $this->getTable()->where('created_at', '<', $expiredAt)->delete();
    }
}