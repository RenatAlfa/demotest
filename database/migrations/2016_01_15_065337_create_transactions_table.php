<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('transaction_id')->unique();
            $table->integer('player_id')->unsigned();
            $table->bigInteger('balance')->unsigned();
            $table->bigInteger('amount')->unsigned();

            $table->enum('type',['BET','WIN','REFUND']);
            $table->timestamp('timestamp');

            $table->foreign('player_id')
                ->references('id')
                ->on('players')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
    }
}
