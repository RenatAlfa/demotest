<?php

use Illuminate\Database\Seeder;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $games = [
            [
                'name' => 'SIZZLING_HOT_DELUXE',
                'uuid' => '6ad19aa9-7317-4411-9e62-bb0b45a8ea1d',
            ],
            [
                'name' => 'FRUITS_AND_ROYALS',
                'uuid' => '0dc5a3a7-e4e4-446b-b354-1cb9f58d25fb',
            ],
            [
                'name' => 'HOLLYWOOD_STAR',
                'uuid' => '30cbb715-8f35-4b14-b3e2-e68aec5d2d28',
            ],
            [
                'name' => 'DIAMOND_SEVEN',
                'uuid' => '8ffed24e-4216-4784-a2c2-f14922ec28e6',
            ],
            [
                'name' => 'DOLPHINS_PEARL',
                'uuid' => 'bf2a636c-28f3-42fb-9f53-1968a17b77e6',
            ],
            [
                'name' => 'DOLPHINS_PEARL_DELUXE',
                'uuid' => '4646f22e-542a-4033-b205-3983f385cdc8',
            ],
            [
                'name' => 'LUCKY_LADYS_CHARM_DELUXE',
                'uuid' => '9a438484-55c3-4207-b2d5-3cc1bff2aef3',
            ],
            [
                'name' => 'SEA_SIRENS',
                'uuid' => 'a3acf66b-314e-4466-ae99-6a850f70c575',
            ],
            [
                'name' => 'THE_REAL_KING_ALOHA_HAWAI',
                'uuid' => 'ee929d2d-d8a9-4cff-a919-f74a38d9c87c',
            ],
            [
                'name' => 'FRUIT_FARM',
                'uuid' => '27f0b8a1-18b5-419e-9f9b-a1e3653b5962',
            ],
            [
                'name' => 'UNICORN_MAGIC',
                'uuid' => 'a5a7ca67-343e-4602-b4a8-ae516bda1837',
            ],
            [
                'name' => 'THE_MONEY_GAME',
                'uuid' => 'e7c79913-848e-40c6-b2ec-f33ab326b09e',
            ],
            [
                'name' => 'PHARAONS_GOLD_2',
                'uuid' => 'b05c03bd-26b0-451f-8537-14f912fb569a',
            ],
            [
                'name' => 'BANANA_SPLASH',
                'uuid' => '6543fbdc-95e7-4ce1-a84c-47551e13ecda',
            ],
            [
                'name' => 'KING_OF_CARDS',
                'uuid' => '42836158-6a15-40e5-8bdc-74714d7f5b3d',
            ],
            [
                'name' => 'ROYAL_TREASURES',
                'uuid' => '62fdd574-0914-4dae-bba4-61fc35a05f82',
            ],
            [
                'name' => 'DYNASTY_OF_MING',
                'uuid' => 'bc5f8b4a-a767-4c57-8759-113a099c5b34',
            ],
            [
                'name' => 'POLAR_FOX',
                'uuid' => '40df30a6-1823-4418-b86d-c7f1e2b34813',
            ],
            [
                'name' => 'WONDERFUL_FLUTE',
                'uuid' => '3dded176-6d42-4122-b0b4-eab5dcdb6172',
            ],
            [
                'name' => 'PANTER_MOON',
                'uuid' => 'ed12d2c2-7eed-4e3e-9bbf-ae54a730e2d2',
            ],
            [
                'name' => 'ATTILA',
                'uuid' => 'f5e399f5-ba6b-4218-b274-acd287743f8e',
            ],
            [
                'name' => 'SPARTA',
                'uuid' => '3a717678-ef75-474f-8ced-808627cbfcab',
            ],
            [
                'name' => 'SAFARI_HEAT',
                'uuid' => 'dfd49792-d78c-4171-8b06-b4696a98f56b',
            ],
            [
                'name' => 'GOLDEN_PLANET',
                'uuid' => '0e47e658-49c0-4b82-b44f-5d46c4d799f2',
            ],
        ];
        foreach ($games as $game)
        {
            DB::insert('INSERT INTO `games`(`uuid`,`name`) VALUES (  :uuid, :name )', $game);
        }
    }
}
