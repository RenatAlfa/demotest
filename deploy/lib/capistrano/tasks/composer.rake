namespace :composer do
    desc "Running Composer Self-Update"
    task :update do
        on roles(:app), in: :sequence, wait: 5 do
            execute :composer, "self-update"
        end
    end

    desc "Running Composer Install"
    task :install do
        on roles(:app), in: :sequence, wait: 5 do
            within release_path  do
                execute :composer, "install --no-dev --quiet"
            end
        end
    end
    desc "Running Composer Install"
    task :dump_autoload do
        on roles(:app), in: :sequence, wait: 5 do
            within release_path  do
                execute :composer, "dump-autoload --optimize"
            end
        end
    end
end