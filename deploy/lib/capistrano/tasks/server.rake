namespace :server do
    desc "restarts php5-fpm"
    task :restart do
        on roles(:all) do
            execute :sudo, :service, "php5-fpm", "restart"
        end
    end
end