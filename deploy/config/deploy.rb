# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'demoproject'
set :repo_url, 'git@bitbucket.org:Inurosen/demotest.git'

set :scm, :git
set :branch, 'master'
set :format, :pretty
set :log_level, :debug
set :linked_files, %w{.env}
set :keep_releases, 5
# set :stages, %w(staging production testing)
# set :default_stage, "testing"

set :deploy_to, '/var/www/demoproject'

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }


namespace :deploy do

    #after :published, "composer:update"
    after :published, "composer:install"
    after :published, "composer:dump_autoload"
    
    after :published, "laravel:permissions"
    after :published, "laravel:migrate"

    #after :published, "laravel:seed"
    after :finishing,  "server:restart"
    after :rollback,   "server:restart"
end